//
//  InAppV2Tests.m
//  InAppV2Tests
//
//  Created by David Díaz on 2/6/14.
//  Copyright (c) 2014 Pademobile. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface InAppV2Tests : XCTestCase

@end

@implementation InAppV2Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
